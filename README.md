# PyOTP

This is very simple otp library which is ported from https://github.com/nathforge/pyotp.

## Installation

```
pip install -e https://minhnhb@bitbucket.org/minhnhb/pyotp.git#egg=pyotp
```

## Usage 

```
from pyotp import TOTP

# This is a secret
secret = "my secret"

# create otp instance. In this example, we will use a time-based otp
otp = TOTP(secret)

# Create an otp
token = otp.now()

# Verify an otp
# The otp should be valid in 60 seconds
otp.verify(token, interval=60)
```