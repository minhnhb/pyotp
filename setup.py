#!/usr/bin/env python
from pyotp import VERSION
from setuptools import setup, find_packages

setup(
    version=VERSION,
    url='https://minhnhb@bitbucket.org/minhnhb/pyotp.git',
    name='pyotp',
    description='Simple otp library',
    packages=find_packages(),
)
